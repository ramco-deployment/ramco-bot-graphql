# Ramco ChiaBot GraphQL Instance Deployment


1. On host machine, create the following folder hierarchy:

```
gql/
 |
 +- docker-compose.yml (based off the docker-compose.yml in this project)
 +- .env (to define environment variables for use in the docker compose)
 +- conf/
      |
      +- config.json (tenant-dependent configurations)
      +- nginx/
           |
           +- logs/
           +- nginx.conf (based off the nginx.conf in this project)
```

2. Open .env and minimally provide the following:

```
ENGINE_API_KEY=...
```

3. `sudo docker-compose up` as test run
4. When okay, `sudo docker-compose down && sudo docker-compose up -d` to run as daemon.
